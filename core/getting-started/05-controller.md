---
# id: halo
# slug: halo
title: Controller
# author: Yangshun Tay
# author_title: Front End Engineer @ Facebook
# author_url: https://github.com/yangshun
# author_image_url: https://avatars0.githubusercontent.com/u/1315101?s=400&v=4
# tags: [facebook, hello, docusaurus]
---

The controller files must be placed in `app/controller` directory. Every controller should extend `CoreController` class or extends another controller that inherits the `CoreController` class. Every controller must be appended with `Controller` following the name. Therefore, a controller named `Page` must have a name `PageController` in its class name declaration and be saved in PHP file named `PageController.php`. Even though it is not mandatory, a controller should have a function named `index()`.

Below is the minimum working example of a controller named `PageController`.

```php title="/app/controller/PageController.php"
<?php 
// make sure there is no blank line or space 
// before PHP code opening tags

class PageController extends CoreController {
    // default method to call when no method is specified
    function index() {
        // print something to the browser
        echo "Hello, the application controller works!";
    }
}

?> // it is also recommended to ommit the PHP closing tag
```
And HTTP request from web browser with the following URL:

```url
http://localhost/index.php/page
  or
http://localhost/index.php/page/index
```

will output the following information on the web browser:

```
Hello, the application controller works!
```

:::caution
The index() function is not mandatory. However, when a request is made without specifying a method, the framework will execute the index() function automatically. In case the specified (default) method could not be found by the framework, it will show an error message.
:::

:::info
If a request neither does not provide the controller name (and also the method name), then the framework will interpret the request to `HomeController` and executes its `index()` method.
:::

## How a Controller method is executed from an HTTP request

An HTTP request is interpreted with the following pattern:

```url
http://hostname/index.php/controller/method/args1/args2/...
```

An HTTP request from the web browser like the following example:

```
http://localhost/index.php/page/introduction/3
```

will be interpreted and routed to:
- Controller: `PageController`
- Method: `introduction`
- Arguments: `3`

Therefore, a PageController should specify the method introduction() and accept one parameter to hold the given argument.

```php title="/app/controller/PageController.php"
...
    function introduction($arg1) {
        // do something with $arg1
        echo "Hi, you give me " . $arg1 . ", thanks!";
    }
...
```

A subsequent arguments separated by slash character will be treated as subsequent arguments. 
Undefined argument(s) will raise a warning message if it does not have default value. Excess argument(s) will be ignored.
Check the following example:

```
http://localhost/index.php/page/introduction/3/practice/2
```
```php title="/app/controller/PageController.php"
...
    // set $arg4 default value to null to avoid warning.
    function introduction($arg1, $arg2, $arg3, $arg4 = NULL) {
        // $arg1 will have value: 3
        // $arg2 will have value: "practice"
        // $arg3 will have value: 2
        // $arg4 will have value: NULL 
        // (because it is not provided in the request URL)
        echo "Please open page {$arg1}, section {$arg2}, ";
        echo "and do exercise {$arg3} from number {$arg4}.";
    }
...
```
The above code will produce the following output:
```php
Please open page 3, section practice, and do excercise 2 from number .
// a null value will be printed as an empty string.
```

:::caution
You cannot specify a method name without specifying the controller name.
If you pass arguments to a method, you must specify both the controller name and method name before the arguments.
:::

## Presenting a web page with View template

Every controller, which extends the CoreController, will have a View object to present and show a View template file. The View object is named `ui` and can be accessed using `$this->ui` reference variable.

The PHP code format to present a view template file is:

```php
$this->ui->view($template, $arrayArgs = [])
```

where `$template` is the file name of the View template and `$arrayArgs` is the array of information passed to the template file.

Let's say in directory `app/view` there is a PHP template file called `hello.php`. The file contains the following HTML code:

```markup title="/app/view/hello.php"
<p>Hello from hello.php.</p>
```

Executing the following PHP code from a Controller:

```php title="/app/controller/PageController.php" {4}
...
// controller method
function introduction() {
    $this->ui->view('hello.php');
}
...
```

will produce the following output on web browser.

```
Hello from hello.php.
```

### Presenting a View template from a View template

From the above example, let's say there is another PHP View template file called `nice.php`. The template can be daisy-chained with a PHP template file `hello.php` like the following example:

```php title="/app/view/hello.php" {6}
<p>Hello from hello.php.</p>
<?php 
    // load nice.php template
    // notice no "ui" on $this variable
    // because a template is already a "ui" object 
    $this->view('nice.php'); 
?>
```

```php title="/app/view/nice.php"
<p>Nice to meet you.</p>
```

Executing the following PHP code from a Controller:

```php title="/app/controller/PageController.php" {4}
...
// controller method
function introduction() {
    $this->ui->view('hello.php');
}
...
```

will produce the following output on the web browser.

```
Hello from hello.php.
Nice to meet you.
```

### Presenting a View template from a subdirectory

If a View template file is located in a subdirectory, the template file can be presented by also including the subdirectory in the name. For example, if a View template is located in `/app/view/sub/subhello.php` then to properly present the template file use the following PHP code:

```php title="/app/controller/PageController.php" {2}
...
$this->ui->view('sub/subhello.php');
...
```
or if presented from `nice.php` View template files
```php title="/app/view/nice.php" {4}
...
// notice no "ui" on $this variable
// because a template is already a "ui" object
$this->view('sub/subhello.php');
...
```

### Passing variables to a View template

Variables can be passed from Controller to a View template the following way:

```php title="/app/controller/PageController.php" {4-6}
...
// controller method
function introduction() {
    $data['name'] = 'Amadeus';
    $data['age'] = 20;
    $this->ui->view('hello.php', $data);
}
...
```
```php title="/app/view/hello.php" {4}
<?php 
    // name will be stored in variable $name
    // age will be stored in variable $age
    echo "Hello {$name}, you are {$age} years old now.";
?>
```

The above codes will produce the following output:

```
Hello Amadeus, you are 20 years old now.
```

## Requesting data from CollectionService

```php title="/app/controller/PageController.php" {8,11}
...
// controller method
function introduction() {
    $name = 'Amadeus';
    $age = 20;
    try {
        // instantiate collection service object
        $cs = new DataCollectionService();
        // get detail data from collection service
        // passing name and age as parameters
        $detail = $cs->getDetail($name, $age);
        // results will be stored in variable $detail
        // do something with $detail 
        // or send it to a template for viewing
        $parameters['detail'] = $detail;
        $this->ui->view('template.php', $parameters);
    } catch(Exception $ex) {
        // something wrong happened, 
        // get the error message from the exception
        $error = $ex->getMessage();
    }
}
...
```

## Throwing an error message

Throwing an error message to web browser from Controller.

```php title="/app/controller/PageController.php"
...
$error = "Access denied.";
CoreError::instance($error)->show();
...
```

## Inherited Methods from CoreController

[See the full description of CoreController API and inherited methods here](/core/api/mvc/core-controller).
