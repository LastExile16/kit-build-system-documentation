---
# id: halo
# slug: halo
title: Directory Structure
# author: Yangshun Tay
# author_title: Front End Engineer @ Facebook
# author_url: https://github.com/yangshun
# author_image_url: https://avatars0.githubusercontent.com/u/1315101?s=400&v=4
# tags: [facebook, hello, docusaurus]
---

The framework uses the following directory structure to start a web application:

```jsx
my-website
├── app/ #main application base directory
│   ├── assets/
│   ├── config/
│   ├── controller/
│   ├── files/
│   ├── model/
│   ├── service/
│   ├── view/
│   └── index.html
├── core-framework
└── index.php #main entry point
```
The `core-framework` directory should be at the same level as the application `app` directory. If you change the location of the `core-framework`, modify its location information in `index.php` entry point file.

And below is the directory structure of the framework itself. 99% of the time you would not need to modify the framework code.

```jsx
my-website
├── app/
└── core-framework/
    ├── config/
    ├── cvs/
    ├── library/
    │    └── drivers/
    ├── index.html
    ├── core.php # application bootloader
    └── README
```

## Application Directory Structure

### `app/assets`

Application assets are placed in this directory; including Javascript and CSS files, icons, and language files. Any files located in this directory can be accessed by the application directly and accessible through web browser. You can freely group each asset category into a specific sub-directory, e.g., `assets/css/` for CSS files or `assets/js/` for Javascript files. There is no limit or convention on how you should manage the assets, so you can freely setup your own assets management to your liking.

<!-- [More information about Assets](assets). -->

### `app/config`

This directory is intended for setting up application configuration settings. Settings with same configuration key with as of core framework configuration will override the framework configuration.

The following configuration file is used for configuring the application:
- `core.json`: overriding the framework configuration
- `db.json`: configuring the database library connection settings. You can configure multiple database connection settings here if your application uses several different credential and/or database server.
- `plugin.json`: used to define a set of javascript/css assets as one ***plugin*** definition, so you can simply include the plugin in just one line of code.

<!-- [More information about Config](config). -->

### `app/controller`

This directory is where you put your application controller files. You can also put the controller into a separate sub-directory for easy controller management.

[More information about Controller](controller).

### `app/files`

This directory is where you put files for downloading or for other static content purposes, such as images, and zip-files.

<!-- [More information about Files](files). -->

### `app/model`

This directory is where you put your application class models. The classes defined here will be treated as generic PHP class. If you already have your own custom-made class from your previous project, you can put the files here. When you instantiate the class, it will be autoloaded by the framework.

[More information about Model](model).

### `app/service`

This directory is where service-oriented classes be located. Most of database-related process classes are put in this directory.

[More information about CollectionService](collection-service).

### `app/view`

Views, or HTML pages are served from this directory. Presenting processed data or information is served by files located in this directory. The common way to present the data with the framework is through generating HTML codes with PHP codes.

[More information about View](view).

---

:::note
The `index.html` file located in the `app` directory is a blank index file to serve when the `app` folder is directly accessed by the web browser.
:::