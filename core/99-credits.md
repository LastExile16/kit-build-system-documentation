---
title: Credits
---

This framework is derived from Panada Framework project. Several extensions has been developed for various purposes and projects; including the system and application framework used in Universitas Brawijaya, Indonesia.

## #Thanks

Thank you are regarded to all developers who contributed to this project.