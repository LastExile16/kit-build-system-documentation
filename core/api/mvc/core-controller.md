---
title: CoreController
---

#### Description

CoreController description.

## Methods

### __construct

#### Prototype

```php
public __construct ()
```

#### Parameters

#### Return Value

---

### redirect

#### Prototype

```php
public redirect (destination)
```

#### Parameters

#### Return Value

---

### location

#### Prototype

```php
public location (path, secure)
```

#### Parameters

#### Return Value

---

### file

#### Prototype

```php
public file (path)
```

#### Parameters

#### Return Value

---

### post

#### Prototype

```php
public post (var, defaultValue)
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
public get (var, defaultValue)
```

#### Parameters

#### Return Value

---

### request

#### Prototype

```php
public request (var, defaultValue)
```

#### Parameters

#### Return Value

---

