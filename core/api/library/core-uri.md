---
title: CoreUri
---

#### Description

CoreUri description.

## Methods

### __construct

#### Prototype

```php
private __construct ()
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance ()
```

#### Parameters

#### Return Value

---

### getScheme

#### Prototype

```php
public getScheme ()
```

#### Parameters

#### Return Value

---

### getHost

#### Prototype

```php
public getHost ()
```

#### Parameters

#### Return Value

---

### getPort

#### Prototype

```php
public getPort ()
```

#### Parameters

#### Return Value

---

### getScript

#### Prototype

```php
public getScript ()
```

#### Parameters

#### Return Value

---

### getQueryString

#### Prototype

```php
public getQueryString ()
```

#### Parameters

#### Return Value

---

### getController

#### Prototype

```php
public getController ()
```

#### Parameters

#### Return Value

---

### getMethod

#### Prototype

```php
public getMethod ()
```

#### Parameters

#### Return Value

---

### getArgs

#### Prototype

```php
public getArgs ()
```

#### Parameters

#### Return Value

---

