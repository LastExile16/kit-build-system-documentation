---
title: CoreLanguage
---

#### Description

CoreLanguage description.

## Methods

### __construct

#### Prototype

```php
public __construct ()
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance ()
```

#### Parameters

#### Return Value

---

### load

#### Prototype

```php
public load (path)
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
public get (key)
```

#### Parameters

#### Return Value

---

### f

#### Prototype

```php
private f ()
```

#### Parameters

#### Return Value

---

