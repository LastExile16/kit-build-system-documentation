---
title: Core
---

#### Description

Core description.

## Methods

### __construct

#### Prototype

```php
private __construct ()
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance (lib)
```

#### Parameters

#### Return Value

---

### __get

#### Prototype

```php
public __get (var)
```

#### Parameters

#### Return Value

---

### getUri

#### Prototype

```php
public getUri (type)
```

#### Parameters

#### Return Value

---

### getConfig

#### Prototype

```php
public getConfig (key)
```

#### Parameters

#### Return Value

---

### loadConfig

#### Prototype

```php
public loadConfig (configFile)
```

#### Parameters

#### Return Value

---

### setMessage

#### Prototype

```php
public setMessage (message, &type)
```

#### Parameters

#### Return Value

---

