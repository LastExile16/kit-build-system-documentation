---
title: QueryBuilderMysql
---

#### Description

QueryBuilderMysql description.

## Methods

### __construct

#### Prototype

```php
public __construct (table, dbConfigKeyOrDb)
```

#### Parameters

#### Return Value

---

### select

#### Prototype

```php
public select (columns)
```

#### Parameters

#### Return Value

---

### selectModel

#### Prototype

```php
public selectModel (modelOrClassName)
```

#### Parameters

#### Return Value

---

### where

#### Prototype

```php
public where (column, opValue, value, connector)
```

#### Parameters

#### Return Value

---

### orWhere

#### Prototype

```php
public orWhere (column, opValue, value)
```

#### Parameters

#### Return Value

---

### whereIn

#### Prototype

```php
public whereIn (column, values)
```

#### Parameters

#### Return Value

---

### whereNotIn

#### Prototype

```php
public whereNotIn (column, values)
```

#### Parameters

#### Return Value

---

### whereNull

#### Prototype

```php
public whereNull (column)
```

#### Parameters

#### Return Value

---

### whereNotNull

#### Prototype

```php
public whereNotNull (column)
```

#### Parameters

#### Return Value

---

### whereBetween

#### Prototype

```php
public whereBetween (column, min, max)
```

#### Parameters

#### Return Value

---

### whereNotBetween

#### Prototype

```php
public whereNotBetween (column, min, max)
```

#### Parameters

#### Return Value

---

### whereColumn

#### Prototype

```php
public whereColumn (columnLeft, opColumnRight, columnRight)
```

#### Parameters

#### Return Value

---

### whereGroup

#### Prototype

```php
public whereGroup (qb, connector)
```

#### Parameters

#### Return Value

---

### whereExists

#### Prototype

```php
public whereExists (qb, connector)
```

#### Parameters

#### Return Value

---

### groupBy

#### Prototype

```php
public groupBy (columns)
```

#### Parameters

#### Return Value

---

### having

#### Prototype

```php
public having (column, opValue, value, connector)
```

#### Parameters

#### Return Value

---

### orHaving

#### Prototype

```php
public orHaving (column, opValue, value)
```

#### Parameters

#### Return Value

---

### insert

#### Prototype

```php
public insert (columnValues, ignore)
```

#### Parameters

#### Return Value

---

### insertIgnore

#### Prototype

```php
public insertIgnore (columnValues)
```

#### Parameters

#### Return Value

---

### insertUpdate

#### Prototype

```php
public insertUpdate (columnValues, updateColumnValues)
```

#### Parameters

#### Return Value

---

### insertModel

#### Prototype

```php
public insertModel (models, columns)
```

#### Parameters

#### Return Value

---

### insertUpdateModels

#### Prototype

```php
public insertUpdateModels (models, columns, updates)
```

#### Parameters

#### Return Value

---

### ignore

#### Prototype

```php
public ignore ()
```

#### Parameters

#### Return Value

---

### delete

#### Prototype

```php
public delete ()
```

#### Parameters

#### Return Value

---

### deleteModel

#### Prototype

```php
public deleteModel (models, keys)
```

#### Parameters

#### Return Value

---

### update

#### Prototype

```php
public update (columnValues)
```

#### Parameters

#### Return Value

---

### updateModel

#### Prototype

```php
public updateModel (model, fields)
```

#### Parameters

#### Return Value

---

### orderBy

#### Prototype

```php
public orderBy (column, order)
```

#### Parameters

#### Return Value

---

### distinct

#### Prototype

```php
public distinct ()
```

#### Parameters

#### Return Value

---

### addSelect

#### Prototype

```php
public addSelect (columns)
```

#### Parameters

#### Return Value

---

### max

#### Prototype

```php
public max (column)
```

#### Parameters

#### Return Value

---

### count

#### Prototype

```php
public count (column)
```

#### Parameters

#### Return Value

---

### queryRaw

#### Prototype

```php
public queryRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### selectRaw

#### Prototype

```php
public selectRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### whereRaw

#### Prototype

```php
public whereRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### orWhereRaw

#### Prototype

```php
public orWhereRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### havingRaw

#### Prototype

```php
public havingRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### orHavingRaw

#### Prototype

```php
public orHavingRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### join

#### Prototype

```php
public join (table, leftColumnOrKeyPairs, rightColumn, operator)
```

#### Parameters

#### Return Value

---

### leftJoin

#### Prototype

```php
public leftJoin (table, leftColumnOrKeyPairs, rightColumn, operator)
```

#### Parameters

#### Return Value

---

### crossJoin

#### Prototype

```php
public crossJoin (table)
```

#### Parameters

#### Return Value

---

### limit

#### Prototype

```php
public limit (offsetOrLimit, limit)
```

#### Parameters

#### Return Value

---

### page

#### Prototype

```php
public page (page, perPage)
```

#### Parameters

#### Return Value

---

### setKey

#### Prototype

```php
public setKey (dbConfigKeyOrDb)
```

#### Parameters

#### Return Value

---

### setData

#### Prototype

```php
public setData (_args)
```

#### Parameters

#### Return Value

---

### getData

#### Prototype

```php
public getData ()
```

#### Parameters

#### Return Value

---

### table

#### Prototype

```php
public table (table)
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
public get ()
```

#### Parameters

#### Return Value

---

### begin

#### Prototype

```php
public begin ()
```

#### Parameters

#### Return Value

---

### commit

#### Prototype

```php
public commit ()
```

#### Parameters

#### Return Value

---

### rollback

#### Prototype

```php
public rollback ()
```

#### Parameters

#### Return Value

---

### execute

#### Prototype

```php
public execute ()
```

#### Parameters

#### Return Value

---

### executeQuery

#### Prototype

```php
public executeQuery (asObject)
```

#### Parameters

#### Return Value

---

### map

#### Prototype

```php
public map (modelOrClassName)
```

#### Parameters

#### Return Value

---

### insertId

#### Prototype

```php
public insertId ()
```

#### Parameters

#### Return Value

---

### result

#### Prototype

```php
public result ()
```

#### Parameters

#### Return Value

---

### getFields

#### Prototype

```php
public getFields ()
```

#### Parameters

#### Return Value

---

### callback

#### Prototype

```php
public callback (functionName, args)
```

#### Parameters

#### Return Value

---

### clear

#### Prototype

```php
public clear (clearType)
```

#### Parameters

#### Return Value

---

### raw

#### Prototype

```php
public static raw (string)
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance (dbConfigKeyOrDb, table)
```

#### Parameters

#### Return Value

---

### bt

#### Prototype

```php
public static bt (column)
```

#### Parameters

#### Return Value

---

### qt

#### Prototype

```php
public static qt (value)
```

#### Parameters

#### Return Value

---

### connector

#### Prototype

```php
public static connector (connectorType)
```

#### Parameters

#### Return Value

---

### esc

#### Prototype

```php
public static esc (value)
```

#### Parameters

#### Return Value

---

### fields

#### Prototype

```php
protected static fields (model)
```

#### Parameters

#### Return Value

---

### getInsertId

#### Prototype

```php
public getInsertId ()
```

#### Parameters

#### Return Value

---

### getAffectedRows

#### Prototype

```php
public getAffectedRows ()
```

#### Parameters

#### Return Value

---

### getInstance

#### Prototype

```php
protected getInstance (key)
```

#### Parameters

#### Return Value

---

### getConfig

#### Prototype

```php
protected static getConfig (configKey)
```

#### Parameters

#### Return Value

---

