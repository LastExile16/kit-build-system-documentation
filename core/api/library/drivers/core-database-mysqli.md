---
title: CoreDatabaseMysqli
---

#### Description

CoreDatabaseMysqli description.

## Methods

### __construct

#### Prototype

```php
public __construct (config, connectionName)
```

#### Parameters

#### Return Value

---

### connect

#### Prototype

```php
public connect ()
```

#### Parameters

#### Return Value

---

### disconnect

#### Prototype

```php
public disconnect ()
```

#### Parameters

#### Return Value

---

### getVersion

#### Prototype

```php
public getVersion ()
```

#### Parameters

#### Return Value

---

### getInsertId

#### Prototype

```php
public getInsertId ()
```

#### Parameters

#### Return Value

---

### getAffectedRows

#### Prototype

```php
public getAffectedRows ()
```

#### Parameters

#### Return Value

---

### getError

#### Prototype

```php
public getError ()
```

#### Parameters

#### Return Value

---

### getLastQuery

#### Prototype

```php
public getLastQuery ()
```

#### Parameters

#### Return Value

---

### begin

#### Prototype

```php
public begin ()
```

#### Parameters

#### Return Value

---

### commit

#### Prototype

```php
public commit ()
```

#### Parameters

#### Return Value

---

### rollback

#### Prototype

```php
public rollback ()
```

#### Parameters

#### Return Value

---

### query

#### Prototype

```php
public query (sql, asObject)
```

#### Parameters

#### Return Value

---

### getVar

#### Prototype

```php
public getVar (query)
```

#### Parameters

#### Return Value

---

### getRow

#### Prototype

```php
public getRow (query)
```

#### Parameters

#### Return Value

---

### escape

#### Prototype

```php
public escape (string)
```

#### Parameters

#### Return Value

---

