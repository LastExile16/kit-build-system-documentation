---
title: CoreSessionNative
---

#### Description

CoreSessionNative description.

## Methods

### __construct

#### Prototype

```php
public __construct (config)
```

#### Parameters

#### Return Value

---

### set

#### Prototype

```php
public set (name, value)
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
public get (name)
```

#### Parameters

#### Return Value

---

### remove

#### Prototype

```php
public remove (name)
```

#### Parameters

#### Return Value

---

### destroy

#### Prototype

```php
public destroy ()
```

#### Parameters

#### Return Value

---

### nocache

#### Prototype

```php
public nocache ()
```

#### Parameters

#### Return Value

---

### id

#### Prototype

```php
public id ()
```

#### Parameters

#### Return Value

---

