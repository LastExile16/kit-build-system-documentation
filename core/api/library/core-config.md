---
title: CoreConfig
---

#### Description

CoreConfig description.

## Methods

### __construct

#### Prototype

```php
private __construct ()
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
public get (key)
```

#### Parameters

#### Return Value

---

### getAll

#### Prototype

```php
public getAll ()
```

#### Parameters

#### Return Value

---

### load

#### Prototype

```php
public load (filepath)
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance ()
```

#### Parameters

#### Return Value

---

### base_url

#### Prototype

```php
public base_url ()
```

#### Parameters

#### Return Value

---

