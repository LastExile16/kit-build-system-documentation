---
id: core-index
slug: index
title: Introduction
author: Yangshun Tay
author_title: Front End Engineer @ Facebook
author_url: https://github.com/yangshun
author_image_url: https://avatars0.githubusercontent.com/u/1315101?s=400&v=4
tags: [facebook, hello, docusaurus]
---

## About Core Framework

Core Framework is a PHP-based web application framework with a small footprint with exceptional performance. It adapts the Model-View-Controller design pattern into Model-CollectionService-Controller-Presenter (MCCP) design pattern to turn a linear web application processing flow into object-oriented programming style.

The framework is best used for your PHP-based web application if:
- You need broad compatibility with standard hosting that runs a variety of PHP version and configuration.
- You want a framework that requires nearly zero configuration.
- You want a framework that does not require you to use the command line.
- You want a ramework that does not require you to adhere to restrictive coding rules.
- You do not want to be forced to learn a templating language - (although a template parser is optionally available if you - desire one).
- You eschew complexity, favoring simple solutions.
- You need clear, thorough documentation.

The framework wraps most of repeating process in generating a web page or a JSON formatted data; minimizing the amount of the neeeded application code. Thus, you can quickly deliver web application in a fast and easy way.

## Requirements

- Apache 2/Nginx web server
- PHP 5.3+/7
- MySQL database server (Optional)

MySQL database server is not necessary, but the framework is optimized for MySQL database server if your application needs one. MariaDB is also compatible.

:::note
Most PHP-based web applications use MySQL database server.With some tweaks on the database library, other DBMS such as PostgreSQL can also be supported.
:::

## Source Repository

The source repository is hosted at bitbucket.org website. Get the latest source at the following URL:

https://bitbucket.org/aryoxp/corefws/src/master

KB: LEL 2020
https://collab.kit-build.net/kb/index.php/home/admin

KB DEMO - LEL 2021
https://collab.kit-build.net/demo/index.php/home/admin

Dynamic Analyzer
https://collab.kit-build.net/jsai/index.php/home/analyzer
